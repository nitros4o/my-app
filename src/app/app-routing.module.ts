import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AsideComponent } from './aside/aside.component';

const routes: Routes = [
  { path: '',      component: HomeComponent },
  { path: 'about/:id',      component: AsideComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
